#ifndef __COMMAND_H__
#define __COMMAND_H__

#include "define.h"

#define CLI_TOKCNT 			8
#define CLI_TOKLEN 			16

void print_ver(void);
void cmd_help(void);
void cmd_reboot(uint8_t argc, char argv[CLI_TOKCNT][CLI_TOKLEN]);
void cmd_motor(uint8_t argc, char argv[CLI_TOKCNT][CLI_TOKLEN]);
void cmd_go(uint8_t argc, char argv[CLI_TOKCNT][CLI_TOKLEN]);
void cmd_pwm(uint8_t argc, char argv[CLI_TOKCNT][CLI_TOKLEN]);

#endif	// __COMMAND_H__
