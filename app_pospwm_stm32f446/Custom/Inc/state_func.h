#ifndef __STATE_FUNC_H__
#define __STATE_FUNC_H__

//#include "define.h"
#include "state_def.h"

state_t state_ent_standby(event_t event);
state_t state_act_standby(event_t event);
void    state_ext_standby(event_t event);

state_t state_ent_fast(event_t event);
state_t state_act_fast(event_t event);
void    state_ext_fast(event_t event);

state_t state_ent_slow(event_t event);
state_t state_act_slow(event_t event);
void    state_ext_slow(event_t event);

#endif
