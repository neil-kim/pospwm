#ifndef __STATE_DEF_H__
#define __STATE_DEF_H__

#include "event_func.h"

typedef enum
{
	STATE_STANDBY=0,
	STATE_FAST,
	STATE_SLOW,
	MAXNUM_STATEs,
	NO_STATE_CHANGE
} state_t;

typedef enum {
	STATE_PHASE_ENTER=0,
	STATE_PHASE_ACTION,
	STATE_PHASE_EXIT,
	MAXNUM_STATE_FUNCs
} state_phase_t;

typedef state_t (*state_entry_func_t)(event_t);
typedef state_t (*state_action_func_t)(event_t);
typedef void (*state_exit_func_t)(event_t);

typedef struct
{
	state_entry_func_t ent_function;
	state_action_func_t act_function;
	state_exit_func_t ext_function;
} state_functions_t;


void init_state_table(void);
void state_init(void);
void states_update(event_t event);
void task_states(void);
void print_event(event_t event);
void print_state(state_phase_t st, state_t state);

state_t get_state(void);
void    set_state(state_t state);

#endif

