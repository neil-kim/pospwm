#ifndef __IO_FUNC_H__
#define __IO_FUNC_H__

#include <stdlib.h>
#include "stm32f4xx_hal.h"
#include "event_func.h"

#define POLL_PERIOD_MS 10
//#define POLL_DEBOUNCE_MS 50
//#define POLL_DEBOUNCE_CNT (POLL_DEBOUNCE_MS/POLL_PERIOD_MS)

#define DEBOUNCE_PRESS_TIME		3  // 50ms
//#define LONG_PRESS_TIME		100 // 1000ms
//#define VLONG_PRESS_TIME		200 // 2000ms

typedef enum {
	BTN_TOOTH=0,
	BTN_HAND,
	MAXNUM_BUTTONSs
} button_t;

typedef enum
{
	PORTA = 0,
	PORTB,
	PORTC,
	PORTD,
	PORTE,
	PORTF,
	MAXNUM_PORTs,
} button_port_t;

typedef enum
{
	POLARITY_ACTIVE_LOW=0,
	POLARITY_ACTIVE_HIGH,
	MAXNUM_POLARITYs,
} button_polarity_t;

typedef struct {
	button_t button_id;
	button_polarity_t polarity;
	GPIO_TypeDef* port;
	uint16_t pin;
	bool pressed;
	bool press_trigger_fired;
	uint32_t press_tick_count;
	event_t event_press;
	event_t event_long;
	event_t event_vlong;
} button_info_t;

typedef enum {
	SWTICH_UNKNOWN=0,
	SWTICH_LOW,
	SWTICH_MID,
	SWTICH_HIGH,
	MAXNUM_SWITCHs
} switch_t;

typedef struct
{
	bool low_batt;
	bool charging;
	bool charging_done;
}
st_port_status_t;

void power_en(bool onoff);
void amp_en(bool onoff);
void reboot(void);
void sleep_mode(void);
void stop_mode(void);
void standby_mode(void);
void poll_button(void);
uint8_t get_vol_switch(void);
void poll_motor(void);
void poll_motor2(void);
void poll_motor3(void);
void poll_motor4(void);

void pwm_set_freq_duty(uint16_t freq, uint16_t duty);

#endif
