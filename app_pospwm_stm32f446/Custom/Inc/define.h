#ifndef __DEFINE_H__
#define __DEFINE_H__

#include "timer_func.h"
#include "led_func.h"
#include "io_func.h"
#include "event_func.h"

//#define UART_DEBUG

#define VER_MAJ 0x00
#define VER_MIN 0x05

#define UART_TX_DELAY 30
#define VOL_MIN 	10
#define VOL_MED 	20
#define VOL_MAX 	31


typedef enum
{
	PULSE_T1_RISE1=0,
	PULSE_T2_FALL,
	PULSE_T3_RISE2,
	PULSE_DONE,
	MAXNUM_PULSE,
} pulse_t;

typedef struct
{
	int pos_target; // duty 100.0 = 1000
	int pos_current;
	uint32_t pos_update_cnt;
} st_motor_t;

typedef struct
{
	bool port_debug;
	st_port_status_t port_io;
	st_led_status_t led[MAXNUM_LED_IDs];
	st_timer_t timer[MAXNUM_TIMERs];
	st_motor_t motor;
} st_status_t;

#endif

