#ifndef __TIMER_FUNC_H__
#define __TIMER_FUNC_H__

#include "main.h"
#include "event_func.h"

typedef enum {
	TIMER_WORKING=0,
	TIMER_TOOTH_PROGRESS,
	TIMER_GENERAL_PURPOSE,
	TIMER_PAUSE,
	MAXNUM_TIMERs
} timers_t;

typedef enum {
	TIMER_ST_RUN=0,
	TIMER_ST_PAUSED,
	TIMER_ST_STOP,
	MAXNUM_TIMER_STs
} timer_status_t;

typedef struct {
	uint32_t timeout_count;
	event_t timeout_event;
	timer_status_t timer_st;
} st_timer_t;


uint32_t get_systick(void);
void set_time_stamp(uint32_t *p);
uint32_t get_time_interval(uint32_t past_set_time_stamp);
void delay_ms(uint32_t msec);

timer_status_t get_timer_status(timers_t timer_id);
void set_timer(timers_t timer_id, uint32_t count_ms, event_t timeout_event);
void reset_timer(timers_t timer_id);
void reset_all_timers(void);
void pause_timer(timers_t timer_id);
void resume_timer(timers_t timer_id);

#endif /* __TIMER_FUNC_H__ */
