#ifndef __EVENT_H__
#define __EVENT_H__

#include <stdbool.h>
#include <stdint.h>

typedef enum
{
	EVENT_1=0,
	EVENT_2,
	EVENT_3,
	EVENT_4,
	MAXNUM_EVENTs,
	EVENT_STATE_POLL,
} event_t;

void event_flag_set(uint8_t event_id);
bool event_flag_get(uint8_t event_id);
void event_flag_clear(uint8_t event_id);
void event_flag_reset(void);
bool event_flag_is_empty(void);
event_t pop_event(void);

#endif

