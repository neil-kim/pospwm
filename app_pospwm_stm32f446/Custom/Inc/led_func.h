#ifndef __LED_FUNC_H__
#define __LED_FUNC_H__

#include <stdbool.h>
#include <stdint.h>

/*
 * ------------------------------------------------
 * low battery alert - on/off LED
 * -----
 * PA.10 = red, HIGH active
 * ------------------------------------------------
 * status - PWM LED
 * -----
 * PA.6 (T3.CH1) = YG, breathing 1.5s up, 1.5s down
 * ------------------------------------------------
 * laser - PWM LED
 * -----
 * PA.7 (T1.CH1N) = 880nm laser led
 * PA.9 (T1.CH2)  = 630nm laser led
*/

typedef enum
{
	LED_COLOR_OFF = 0,
	LED_COLOR_RED,
	LED_COLOR_WHITE,
	MAXNUM_LED_COLORs
} led_color_t;

typedef enum
{
	LED_STYLE_SOLID = 0,
	LED_STYLE_BLINK,
	MAXNUM_LED_STYLEs
} led_style_t;

typedef enum
{
	LED_R = 0,
	MAXNUM_LED_UNITs
} led_unit_id_t;

typedef enum
{
	LED_TOOTH1=0,
	LED_TOOTH2,
	LED_TOOTH3,
	LED_TOOTH4,
	LED_TOOTH5,
	LED_TOOTH6,
	LED_HAND,
	MAXNUM_LED_IDs
} led_id_t;

typedef enum
{
	PWMLED_STYLE_SOLID = 0,
	PWMLED_STYLE_BREATHE,
	MAXNUM_PWMLED_STYLEs
} pwmled_style_t;

typedef enum
{
	PWMLED_ID_STAT=0,
	LED_ID_LASER_630,
	LED_ID_LASER_880,
	MAXNUM_PWMLED_IDs
} pwmled_id_t;

typedef struct
{
	bool 	active;
	led_style_t action; 		// blink, solid
	led_color_t solid_color;	// set led color for solid mode
	led_color_t blink_color[2];	// set 2 led colors for blink mode - 0:fore, 1:back
	uint8_t blink_index; 	// on or off for blink mode - o:on, 1:off
	uint32_t blink_period;	// blinking (half) period
	uint32_t blink_tsamp;	// time stamp to control blink
	uint32_t action_timeout_ms;	// turn off led after specified time, 0 means not specified(infinite)
	uint32_t action_tstamp;	// time stamp to control turn off timer
}
st_led_status_t;

typedef struct
{
	bool 	active;
	pwmled_style_t action; 		// solid, breathe
	bool solid_onoff;
	uint16_t intensity_curr;
	uint16_t intensity_min;
	uint16_t intensity_max;
	bool ramping; 	// ramping up or down for breathing mode - o:ramp down, 1:ramp up
	uint32_t ramp_period;	// breathing (half) period
	uint32_t ramp_tsamp;	// time stamp to control blink
	uint32_t action_timeout_ms;	// turn off led after specified time, 0 means not specified(infinite)
	uint32_t action_tstamp;	// time stamp to control turn off timer
} st_pwmled_status_t;


void led_set_color(led_id_t idx, led_color_t color);
void set_led_action_blink(led_id_t idx, led_color_t color_fore, led_color_t color_back, uint32_t period, uint32_t timeout);
void set_led_action_solid(led_id_t idx, led_color_t color, uint32_t timeout);
void set_led_action_stop(led_id_t idx);
void set_led_action_stop_all(void);
void set_led_action_tooth_progress(uint8_t progress);
void set_led_action_tooth_progress_pause(uint8_t progress);

#if 0
void pwm_set_freq_duty(uint8_t idx, uint16_t freq, uint16_t duty);
void pwm_start(uint8_t idx);
void pwm_stop(uint8_t idx);
void set_pwmled_action_solid(pwmled_id_t idx, bool onoff, uint32_t timeout);
void set_pwmled_action_stop(pwmled_id_t idx);
void set_pwmled_action_breathe(pwmled_id_t idx, uint32_t period, uint32_t timeout);
#endif

#endif
