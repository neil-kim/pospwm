#ifndef __MPRINTF_H__
#define __MPRINTF_H__

int mprintf(const char *format, ...);
int msprintf(char *out, const char *format, ...);

#endif
