#ifndef __TASK_FUNC_H__
#define __TASK_FUNC_H__

#include "main.h"
#include "task_func.h"

void task_console(void);
void task_poll(void);
void task_led_player(void);
void task_pwm_player(void);
void task_timer(void);

#endif
