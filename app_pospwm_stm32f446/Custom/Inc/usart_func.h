#ifndef __USART_FUNC_H__
#define __USART_FUNC_H__

#include <stdint.h>
#include <stdbool.h>
#include "main.h"

#define UART0_RX_QUE_SIZE 	16 //64
#define CONSOLE_BUFF_SIZE	UART0_RX_QUE_SIZE

int uart_putc(int ch);
void reset_uart_rx_que(void);
void dec_uart_rx_que(void);
void inc_uart_rx_que(void);
void push_uart_rx_que(uint8_t *p_rxd);
int pop_uart_rx_que(uint8_t *c);

#endif /* __USART_FUNC_H__ */
