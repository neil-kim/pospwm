#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "stm32f4xx_hal.h"

#include "define.h"
#include "main.h"
#include "command.h"
//#include "controller.h"
//#include "tim.h"
#include "event_func.h"
#include "state_def.h"

extern st_status_t g_st;

void print_ver(void)
{
	printf("\n");
	printf("=================================\n");
	printf(" usec tick application (v%d.%d)\n", VER_MAJ,VER_MIN);
	printf("=================================\n");
}

//------------------------------------------------------------------------------------
void cmd_help(void)
{
	printf("commands: \n");
	printf("  ver\n");
	printf("  debug on|off\n");
	printf("  nv dump\n");
	printf("  pump (duty [<percent>])|(<idx> <freq> <duty>)\n");
	printf("  reboot\n");
	printf("  tool temp|verify|reset|uid|(serial [<sn>])|(heater on|off)\n");
	printf("\n");
}


//------------------------------------------------------------------------------------
void cmd_reboot(uint8_t argc, char argv[CLI_TOKCNT][CLI_TOKLEN])
{
	reboot();
}

//------------------------------------------------------------------------------------
// motor (on|off)|[(cw|ccw)|[<duty>]]
// motor ccw
void cmd_motor(uint8_t argc, char argv[CLI_TOKCNT][CLI_TOKLEN])
{
	bool on, cw;
	int8_t duty;

	if(argc>=2)
	{
		if(strcmp(argv[1], "on")!=0 || strcmp(argv[1], "off")!=0)
		{
			cmd_help();
			return;
		}
		on = (strcmp(argv[1], "on")==0)? true: false;
		printf("motor onoff=%d ", on);
	}
	if(argc>=3)
	{
		if(strcmp(argv[2], "cw")!=0 || strcmp(argv[2], "ccw")!=0)
		{
			cmd_help();
			return;
		}
		cw = (strcmp(argv[2], "cw")==0)? true: false;
		printf("cw=%d ", cw);
	}
	if(argc>=4)
	{
		duty = atoi(argv[3]);
		if(duty<0 || 100<duty)
		{
			cmd_help();
			return;
		}
		printf("duty=%d ", duty);
	}
	printf("\n");

//		if(argc==2)
//			do onoff
//		if(argc==3)
//			do onoff, do direction
//		if(argc==4)
//			do onoff, do direction, do duty

	cmd_help();
	return;
}

//------------------------------------------------------------------------------------
// go <duty>
void cmd_go(uint8_t argc, char argv[CLI_TOKCNT][CLI_TOKLEN])
{
	int8_t duty;

	if(argc>=2)
	{
		duty = atoi(argv[3]);
		if(duty<10 || 90<duty)
		{
			cmd_help();
			return;
		}
		printf("duty=%d\n", duty);
		return;
	}

	return;
}

//------------------------------------------------------------------------------------
// pwm <freq> <duty>
void cmd_pwm(uint8_t argc, char argv[CLI_TOKCNT][CLI_TOKLEN])
{
	int freq, duty;

	if(argc==3)
	{
		freq = atoi(argv[1]);
		duty = atoi(argv[2]);
		if(duty<0 || 100<duty)
		{
			cmd_help();
			return;
		}
		pwm_set_freq_duty(freq, duty);
		printf("freq=%d, duty=%d\n", freq, duty);
		return;
	}

	cmd_help();
	return;
}

#if 0
//------------------------------------------------------------------------------------
void cmd_nv_usage(void)
{
	cmd_help();
}

void cmd_nv(uint8_t argc, char argv[CLI_TOKCNT][CLI_TOKLEN])
{
	if(argc==2)
	{
		if(strcmp(argv[1], "dump")==0)
		{
			read_nvram(&g_nv, true);
			nvram_check_valid(&g_nv, true);
			return;
		}
	}

	cmd_nv_usage();
	return;
}

//------------------------------------------------------------------------------------
void cmd_debug_usage(void)
{
	cmd_help();
}

void cmd_debug(uint8_t argc, char argv[CLI_TOKCNT][CLI_TOKLEN])
{
	if(argc==2)
	{
		g_st.port_debug = (strcmp(argv[1], "on")==0)? true: false;
		printf("debug=%s\n", argv[1]);
		return;
	}
	cmd_debug_usage();
	return;
}

#endif
