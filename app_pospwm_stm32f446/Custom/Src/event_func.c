//#include <stdio.h>
#include <stdint.h>
#include "event_func.h"


static uint64_t event_flags;

void event_flag_set(uint8_t event_id)
{
	if(event_id<64)
	{
		event_flags |= (1UL<<event_id);
	}
}

bool event_flag_get(uint8_t event_id)
{
	bool ret=false;

	if(event_id<64)
	{
		ret = (event_flags & (1UL<<event_id))? true: false;
	}

	return ret;
}

void event_flag_clear(uint8_t event_id)
{
	if(event_id<64)
	{
		event_flags &= ~(1UL<<event_id);
	}
}

void event_flag_reset(void)
{
	event_flags = 0l;
}

bool event_flag_is_empty(void)
{
	return (event_flags)? false: true;
}

event_t pop_event(void)
{
	int i;

	if(event_flag_is_empty())
		return EVENT_STATE_POLL;

	for(i=0; i<MAXNUM_EVENTs; i++) // 0=highest priority, 31=lowest prioty
	{
		if(event_flag_get(i))
		{
			event_flag_clear(i);
			return (event_t)i;
		}
	}

	return EVENT_STATE_POLL;
}


