//#include "console.h"
#include "define.h"
#include "usart_func.h"
#include "task_func.h"
#include "state_func.h"
#include "console.h"
//#include "mprintf.h"

extern st_status_t g_st;

void task_console(void)
{
	uint8_t c;

	if( pop_uart_rx_que(&c) == 0 )
	{
		readline(c);
	}
}


void task_poll(void)
{
	static uint32_t tstamp_poll;

	if( get_time_interval(tstamp_poll) >= POLL_PERIOD_MS )
	{
		set_time_stamp(&tstamp_poll);
//		poll_button();
//		poll_motor();

		poll_motor4();
	}

}

#if 0
void task_led_player(void)
{
	int idx;

	for(idx=0; idx<MAXNUM_LED_IDs; idx++)
	{
		if(!g_st.led[idx].active) continue;

		//blink
		if(g_st.led[idx].action == LED_STYLE_BLINK)
		{
			if(get_time_interval(g_st.led[idx].blink_tsamp) >= g_st.led[idx].blink_period)
			{
				set_time_stamp(&g_st.led[idx].blink_tsamp);
				g_st.led[idx].blink_index ^= 1;

				led_set_color((led_id_t)idx, g_st.led[idx].blink_color[ g_st.led[idx].blink_index ] );
			}
		}

		// solid on / off
		if(g_st.led[idx].action == LED_STYLE_SOLID)
		{
			led_set_color((led_id_t)idx, g_st.led[idx].solid_color);
		}

		// led off after timeout - both of solid and blink
		if(g_st.led[idx].active && g_st.led[idx].action_timeout_ms!=0)
		{
			if(get_time_interval(g_st.led[idx].action_tstamp) >= g_st.led[idx].action_timeout_ms)
			{
				g_st.led[idx].active = false;
				g_st.led[idx].solid_color = LED_COLOR_OFF;
				g_st.led[idx].action_timeout_ms = 0;

				led_set_color((led_id_t)idx, LED_COLOR_OFF);
			}
		}
	}
}
#endif

void task_timer(void)
{
	int i;
	static uint32_t tstamp_timer;

	if( get_time_interval(tstamp_timer) >= 1 )
	{
		set_time_stamp(&tstamp_timer);

		for(i=0; i<MAXNUM_TIMERs; i++)
		{
			if( (get_timer_status((timers_t)i)!=TIMER_ST_PAUSED) && (0 < g_st.timer[i].timeout_count) )
			{
				g_st.timer[i].timeout_count--;
				if(g_st.timer[i].timeout_count == 0)
				{
					event_flag_set(g_st.timer[i].timeout_event);
					reset_timer((timers_t)i);
				}
			}
		}
	}
}



void task_states(void)
{
	event_t event;

	event = pop_event();
	if(event != EVENT_STATE_POLL)
	{
		print_event(event);
	}

	states_update(event);
}

