#include <stdio.h>
#include <stdbool.h>
#include "define.h"
#include "main.h"
#include "io_func.h"
#include "event_func.h"
#include "mprintf.h"

extern st_status_t g_st;
extern uint8_t 	g_uart_rxd;
/*
extern volatile uint32_t g_usec_tick7;
extern volatile uint32_t g_usec_tick11;
extern volatile uint32_t usec_t1_rise1, usec_t2_fall, usec_t3_rise2;
extern volatile pulse_t measure_step; //0=release, 1=rising1 done, 2=falling done, 3=rising2 done
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim7;
*/

void motor_en(bool onoff, bool dir, uint8_t pwm_duty)
{
/*
	GPIO_PinState st;
	st = (onoff)? GPIO_PIN_SET: GPIO_PIN_RESET;
	HAL_GPIO_WritePin(PWR_EN_GPIO_Port, PWR_EN_Pin, st);
*/
}

void reboot(void)
{
	HAL_Delay(100);
	HAL_NVIC_SystemReset();
}

void sleep_mode(void)
{
#ifdef UART_DEBUG
	mprintf("sleep mode\n");
#endif
	HAL_SuspendTick();
	__HAL_RCC_PWR_CLK_ENABLE();
	HAL_PWR_EnterSLEEPMode(0, PWR_SLEEPENTRY_WFI);
	// enter sleep mode ...
	// -------------------
	// ... wake up with interrupt/event
	HAL_ResumeTick();
#ifdef UART_DEBUG
	mprintf("wakeup\n");
#endif
}

void SystemClock_Config(void); //at main.c
void stop_mode(void)
{
#ifdef UART_DEBUG
	mprintf("stop mode\n");
#endif
	HAL_SuspendTick();
	__HAL_RCC_PWR_CLK_ENABLE();
	HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);
	// enter stop mode ...
	// -------------------
	// ... wake up with interrupt/event
	HAL_ResumeTick();
	SystemClock_Config();
#ifdef UART_DEBUG
	mprintf("wakeup\n");
#endif
}

void standby_mode(void)
{
#ifdef UART_DEBUG
	mprintf("standby mode\n");
#endif
	// wait for some time
	HAL_Delay(5000);
	__HAL_RCC_PWR_CLK_ENABLE();
	HAL_PWR_EnterSTANDBYMode();
	// enter standby mode
	// -------------------
	// ... wake up with reset / iwdg / rtc wakeup
#ifdef UART_DEBUG
	mprintf("wakeup\n");
#endif
}

extern uint32_t usec_fall, usec_rise;

extern uint32_t capt_time_stamp[20];
extern uint8_t capt_cnt;

void poll_motor4(void)
{
//	printf("%ld, %ld, %ld\n", usec_rise, usec_fall, usec_rise*1000 / usec_fall);
/*
	int i;
	if(capt_cnt>=20)
	{
		for(i=0;i<20;i++)
			printf("%ld ", capt_time_stamp[i]);
		printf("\n");

		capt_cnt=0;
	}
*/
}

void pwm_set_freq_duty(uint16_t freq, uint16_t duty)
{
	uint32_t psc, arr, ccr, fclk;

	fclk = 84000000;
	psc = 84;

	arr = fclk / (freq * psc);
	ccr = duty * arr / 100;
/*
	__HAL_TIM_SET_PRESCALER(&htim2, psc-1);
	__HAL_TIM_SET_AUTORELOAD(&htim2, (arr>0)? (arr-1): 0);
	__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, (ccr>0)? (ccr-1): 0);
*/
}

#if 0
// The order here must match the order in switch_t
static button_info_t buttons[MAXNUM_BUTTONSs] =
{
	{
		.button_id 	= BTN_TOOTH,
		.polarity 	= POLARITY_ACTIVE_LOW,
		.port 		= KEY_TOOTH_GPIO_Port,
		.pin 		= KEY_TOOTH_Pin,
		.event_press= EVENT_BTN_TOOTH,
	},
	{
		.button_id 	= BTN_HAND,
		.polarity 	= POLARITY_ACTIVE_LOW,
		.port 		= KEY_HAND_GPIO_Port,
		.pin 		= KEY_HAND_Pin,
		.event_press= EVENT_BTN_HAND,
	},
};
#endif

#if 0
void poll_button(void)
{
	// update physical button status
	for(int i=0; i<MAXNUM_BUTTONSs; i++)
	{
		bool high = HAL_GPIO_ReadPin(buttons[i].port, buttons[i].pin);
		if ( 	( high && buttons[i].polarity == POLARITY_ACTIVE_HIGH) ||
				(!high && buttons[i].polarity == POLARITY_ACTIVE_LOW ) 	)
		{
			// pressed
			if(!buttons[i].pressed)
			{
				buttons[i].press_tick_count = 0;
				buttons[i].pressed = true;
			}
		}
		else
		{
			// released
			if(buttons[i].pressed)
			{
				// release event
				buttons[i].pressed = false;
			}
		}
	}

	// check logical button status
	for(int i=0; i<MAXNUM_BUTTONSs; i++)
	{
		// Tick up only if we haven't already hit long press time, preventing
		// multiple fires of the hold callback
		if((buttons[i].pressed) /*&& (buttons[i].press_tick_count < LONG_PRESS_TIME)*/)
		{
			buttons[i].press_tick_count++;

			if(buttons[i].press_tick_count == DEBOUNCE_PRESS_TIME)
			{
				buttons[i].press_trigger_fired = true;
				// press event
				event_flag_set(buttons[i].event_press);
			}
#if 0 //reserved
			else if(buttons[i].press_tick_count == LONG_PRESS_TIME)
			{
				// long press event
				event_flag_set(buttons[i].event_long);
			}
			else if(buttons[i].press_tick_count == VLONG_PRESS_TIME)
			{
				// long press event
				event_flag_set(buttons[i].event_vlong);
			}
			else if(buttons[i].press_tick_count == VVLONG_PRESS_TIME)
			{
				// vvlong press event
				event_flag_set(buttons[i].event_vvlong);
			}
			else if( (buttons[i].press_tick_count > VVLONG_PRESS_TIME) && (buttons[i].press_tick_count % LONG_REPEAT_TIME == 0) )
			{
				// very repeat event
				event_flag_set(buttons[i].event_vrepeat);
			}
			else if( (buttons[i].press_tick_count > LONG_PRESS_TIME) && (buttons[i].press_tick_count % LONG_REPEAT_TIME == 0) )
			{
				// repeat event
				event_flag_set(buttons[i].event_repeat);
			}
#endif
		}
	}
}
#endif

#if 0
// milisec tick count from systick
extern volatile uint32_t UptimeMillis;
static inline uint32_t GetMicros()
{
    uint32_t ms;
    uint32_t st;

    do
    {
        ms = UptimeMillis;
        st = SysTick->VAL;
        asm volatile("nop");
        asm volatile("nop");
    } while (ms != UptimeMillis);

    return ms * 1000 - st / ((SysTick->LOAD + 1) / 1000);
}

static inline uint32_t GetMicrosFromISR()
{
    uint32_t st = SysTick->VAL;
    uint32_t pending = SCB->ICSR & SCB_ICSR_PENDSTSET_Msk;
    uint32_t ms = UptimeMillis;

    if (pending == 0)
        ms++;

    return ms * 1000 - st / ((SysTick->LOAD + 1) / 1000);
}
#endif

