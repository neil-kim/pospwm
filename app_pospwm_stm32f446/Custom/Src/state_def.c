#include <stdio.h>
#include "state_def.h"
#include "state_func.h"
#include "define.h"
#include "io_func.h"
#include "led_func.h"
#include "event_func.h"
#include "timer_func.h"
//#include "mprintf.h"

uint32_t state_tstamp;
state_t current_state;

const state_functions_t state_table[MAXNUM_STATEs] =
{
	{	// STATE_STANDBY,
		.ent_function 	= state_ent_standby,
		.act_function 	= state_act_standby,
		.ext_function 	= state_ext_standby
	},
	{	// STATE_FAST,
		.ent_function 	= state_ent_fast,
		.act_function 	= state_act_fast,
		.ext_function 	= state_ext_fast
	},
	{	// STATE_SLOW,
		.ent_function 	= state_ent_slow,
		.act_function 	= state_act_slow,
		.ext_function 	= state_ext_slow
	},
};


// -------------------------------------------------------------
void state_init()
{
//	init_state_table();
	set_state(STATE_STANDBY);
	state_ent_standby(EVENT_STATE_POLL);
}

void states_update(event_t event)
{
	state_t new_state;

	if(state_table[get_state()].act_function != NULL)
	{
		new_state = state_table[get_state()].act_function(event);
	}
	else
	{
		// never come here
		state_init();
		return;
	}

	while(new_state != NO_STATE_CHANGE)
	{
		state_tstamp = get_systick();
		if(state_table[get_state()].ext_function != NULL)
		{
			print_state(STATE_PHASE_EXIT, get_state());
			state_table[get_state()].ext_function(event);
		}

		set_state(new_state);
		new_state = NO_STATE_CHANGE;
		if(state_table[get_state()].ent_function != NULL)
		{
			print_state(STATE_PHASE_ENTER, get_state());
			new_state = state_table[get_state()].ent_function(event);
		}
	}
}

void print_event(event_t event)
{
	printf("%ld <EVENT> ", get_systick());

	switch(event)
	{
		case EVENT_1:
			printf("EVENT_1");
			break;
		case EVENT_2:
			printf("EVENT_2");
			break;
		case EVENT_3:
			printf("EVENT_3");
			break;
		case EVENT_4:
			printf("EVENT_4");
			break;

		default:
			printf("UNDEFINED(%d)", event);
			break;
	}
	printf("\n");
}

void print_state(state_phase_t st, state_t state)
{
	printf("%ld [STATE] ", get_systick());
	switch(st)
	{
		case STATE_PHASE_ENTER:
			printf("ENTER ");
			break;
		case STATE_PHASE_ACTION:
			printf("ACTION ");
			break;
		case STATE_PHASE_EXIT:
			printf("EXIT  ");
			break;
		default:
			printf("UNDEFINED");
			break;
	}
	switch(state)
	{
		case STATE_STANDBY:
			printf("STATE_STANDBY");
			break;
		case STATE_FAST:
			printf("STATE_FAST");
			break;
		case STATE_SLOW:
			printf("STATE_SLOW");
			break;

		default:
			printf("UNDEFINED");
			break;
	}
	printf("\n");
}

// -------------------------------------------------------------
state_t get_state(void)
{
	return current_state;
}
void set_state(state_t state)
{
	current_state = state;
}

