#include "stm32f4xx.h"
#include "define.h"
#include "led_func.h"
#include "timer_func.h"

extern st_status_t g_st;
#if 0
void led_set_color(led_id_t idx, led_color_t color)
{
	GPIO_TypeDef* GPIOx;
	uint16_t GPIO_Pin;
	GPIO_PinState PinState;
	PinState = (color==LED_COLOR_OFF)? GPIO_PIN_RESET: GPIO_PIN_SET;

	if(idx>=MAXNUM_LED_IDs) return;

	switch(idx)
	{
		case LED_TOOTH1: GPIOx=LED1_EN_GPIO_Port; GPIO_Pin=LED1_EN_Pin; break;
		case LED_TOOTH2: GPIOx=LED2_EN_GPIO_Port; GPIO_Pin=LED2_EN_Pin; break;
		case LED_TOOTH3: GPIOx=LED3_EN_GPIO_Port; GPIO_Pin=LED3_EN_Pin; break;
		case LED_TOOTH4: GPIOx=LED4_EN_GPIO_Port; GPIO_Pin=LED4_EN_Pin; break;
		case LED_TOOTH5: GPIOx=LED5_EN_GPIO_Port; GPIO_Pin=LED5_EN_Pin; break;
		case LED_TOOTH6: GPIOx=LED6_EN_GPIO_Port; GPIO_Pin=LED6_EN_Pin; break;
		case LED_HAND:	 GPIOx=LED7_EN_GPIO_Port; GPIO_Pin=LED7_EN_Pin; break;
		default: return;
	}

	HAL_GPIO_WritePin(GPIOx, GPIO_Pin, PinState);
}

void set_led_action_blink(led_id_t idx, led_color_t color_fore, led_color_t color_back, uint32_t period, uint32_t timeout)
{
	int i;

	if(	(idx>=MAXNUM_LED_IDs) || (color_fore >= MAXNUM_LED_COLORs || color_back >= MAXNUM_LED_COLORs) )
		return;

	led_set_color(idx, color_fore);

	g_st.led[idx].active = true;
	g_st.led[idx].action = LED_STYLE_BLINK;
	g_st.led[idx].blink_color[0] = color_fore;
	g_st.led[idx].blink_color[1] = color_back;
	g_st.led[idx].blink_period = period;
	g_st.led[idx].action_timeout_ms = timeout;
	set_time_stamp(&g_st.led[idx].action_tstamp);

	for(i=0;i<MAXNUM_LED_IDs;i++)
	{
		set_time_stamp(&g_st.led[i].blink_tsamp);
		g_st.led[i].blink_index = 0;
	}
}

void set_led_action_solid(led_id_t idx, led_color_t color, uint32_t timeout)
{
	if(	(idx>=MAXNUM_LED_IDs) || (color >= MAXNUM_LED_COLORs ) )
		return;

	led_set_color(idx, color);

	g_st.led[idx].active = true;
	g_st.led[idx].action = LED_STYLE_SOLID;
	g_st.led[idx].solid_color = color;
	g_st.led[idx].action_timeout_ms = timeout;
	set_time_stamp(&g_st.led[idx].action_tstamp);
}

void set_led_action_stop(led_id_t idx)
{
	if(idx>=MAXNUM_LED_IDs)
		return;

	led_set_color(idx, LED_COLOR_OFF);

	g_st.led[idx].active = false;
	g_st.led[idx].action_timeout_ms = 0;
	set_time_stamp(&g_st.led[idx].action_tstamp);
}

void set_led_action_stop_all(void)
{
	int idx;
	for(idx=0; idx<MAXNUM_LED_IDs; idx++)
	{
		set_led_action_stop(idx);
	}
}

void set_led_action_tooth_progress(uint8_t progress)
{
	switch(progress)
	{
		case 1:
			// led 1, 20sec
			set_led_action_solid(LED_HAND, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH1, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH2, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH3, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH4, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH5, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH6, LED_COLOR_OFF, 0);
			break;

		case 2:
			// led 2, 20sec
			set_led_action_solid(LED_HAND, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH1, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH2, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH3, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH4, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH5, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH6, LED_COLOR_OFF, 0);
			break;

		case 3:
			// led 2, 20sec
			set_led_action_solid(LED_HAND, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH1, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH2, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH3, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH4, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH5, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH6, LED_COLOR_OFF, 0);
			break;

		case 4:
			// led 3, 20sec
			set_led_action_solid(LED_HAND, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH1, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH2, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH3, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH4, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH5, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH6, LED_COLOR_OFF, 0);
			break;

		case 5:
			// led 4, 20sec
			set_led_action_solid(LED_HAND, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH1, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH2, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH3, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH4, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH5, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH6, LED_COLOR_OFF, 0);
			break;

		case 6:
			// led 5, 20sec
			set_led_action_solid(LED_HAND, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH1, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH2, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH3, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH4, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH5, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH6, LED_COLOR_WHITE, 0);
			break;

		case 0:
		default:
			set_led_action_stop_all();
			break;
	}
}

void set_led_action_tooth_progress_pause(uint8_t progress)
{
	switch(progress)
	{
		case 1:
			// led 1, 20sec
			set_led_action_solid(LED_HAND, LED_COLOR_OFF, 0);
			set_led_action_blink(LED_TOOTH1, LED_COLOR_WHITE, LED_COLOR_OFF, 500, 0);
			set_led_action_solid(LED_TOOTH2, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH3, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH4, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH5, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH6, LED_COLOR_OFF, 0);
			break;

		case 2:
			// led 2, 20sec
			set_led_action_solid(LED_HAND, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH1, LED_COLOR_WHITE, 0);
			set_led_action_blink(LED_TOOTH2, LED_COLOR_WHITE, LED_COLOR_OFF, 500, 0);
			set_led_action_solid(LED_TOOTH3, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH4, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH5, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH6, LED_COLOR_OFF, 0);
			break;

		case 3:
			// led 2, 20sec
			set_led_action_solid(LED_HAND, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH1, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH2, LED_COLOR_WHITE, 0);
			set_led_action_blink(LED_TOOTH3, LED_COLOR_WHITE, LED_COLOR_OFF, 500, 0);
			set_led_action_solid(LED_TOOTH4, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH5, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH6, LED_COLOR_OFF, 0);
			break;

		case 4:
			// led 3, 20sec
			set_led_action_solid(LED_HAND, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH1, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH2, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH3, LED_COLOR_WHITE, 0);
			set_led_action_blink(LED_TOOTH4, LED_COLOR_WHITE, LED_COLOR_OFF, 500, 0);
			set_led_action_solid(LED_TOOTH5, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH6, LED_COLOR_OFF, 0);
			break;

		case 5:
			// led 4, 20sec
			set_led_action_solid(LED_HAND, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH1, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH2, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH3, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH4, LED_COLOR_WHITE, 0);
			set_led_action_blink(LED_TOOTH5, LED_COLOR_WHITE, LED_COLOR_OFF, 500, 0);
			set_led_action_solid(LED_TOOTH6, LED_COLOR_OFF, 0);
			break;

		case 6:
			// led 5, 20sec
			set_led_action_solid(LED_HAND, LED_COLOR_OFF, 0);
			set_led_action_solid(LED_TOOTH1, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH2, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH3, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH4, LED_COLOR_WHITE, 0);
			set_led_action_solid(LED_TOOTH5, LED_COLOR_WHITE, 0);
			set_led_action_blink(LED_TOOTH6, LED_COLOR_WHITE, LED_COLOR_OFF, 500, 0);
			break;

		case 0:
		default:
			set_led_action_stop_all();
			break;
	}
}
#endif

