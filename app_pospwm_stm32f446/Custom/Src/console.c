#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "main.h"
#include "usart_func.h"
#include "console.h"
#include "command.h"

char g_console_buffer[CONSOLE_BUFF_SIZE] = {0};
unsigned int g_console_idx = 0;

void reset_console_buffer(void)
{
	g_console_idx = 0;
	memset(g_console_buffer, 0x00, CONSOLE_BUFF_SIZE);
	reset_uart_rx_que();
}

#define CFG_PROMPT "> "
#define erase_seq	"\b \b"

//------------------------------------------------------------------------------------
void readline(char c)
{
	switch (c)
	{
		case '\n' :
			break;
		case '\r' :	// user pressed ENTER key - line input finished
			printf("\n");
			g_console_buffer[g_console_idx] = '\0';
			parseline(g_console_buffer);
			reset_console_buffer();
			printf(CFG_PROMPT);
			break;

		case 0x03 :	// ^C - break
			reset_console_buffer();
			printf(CFG_PROMPT);
			break;

		case 0x08 :	// ^H  - backspace
		case 0x7F :	// DEL - break
			if(g_console_idx)
			{
				printf(erase_seq);
				g_console_idx--;
			}
			break;

		default :
			if (g_console_idx < CONSOLE_BUFF_SIZE)
			{
				g_console_buffer[g_console_idx++] = c;
				uart_putc(c);
			}
			else
			{
				// Buffer full
				uart_putc(c);
				reset_console_buffer();
				printf(CFG_PROMPT);
			}
			break;
	}
}


//------------------------------------------------------------------------------------
void parseline(char *str)
{
	char	*ptr;
	uint8_t argc;
	char argv[CLI_TOKCNT][CLI_TOKLEN];

//	for(i=0; i<strlen(str); i++)
//		str[i] = tolower(str[i]);

	memset(argv, 0x00, CLI_TOKCNT*CLI_TOKLEN);
	argc = 0;

	if( (ptr = strtok( str, " ")) != NULL )
	{
		strncpy(argv[argc], ptr, CLI_TOKLEN);
		argc++;
	}

	while( (ptr = strtok( NULL, " ")) != NULL )
	{
		strncpy(argv[argc], ptr, CLI_TOKLEN);
		argc++;
	}

#if 0 // debug command line input & parsing
	{
		int i;
		printf("argc=%d\n\r", argc);
		for(i=0;i<argc;i++)
			printf("argv[%d]=%s\n\r", i, argv[i]);
	}
#endif

	//-----------------------------------------------------
	if( strcmp(argv[0], "help")==0 || strcmp(argv[0], "?")==0 )
	{
		cmd_help();
		return;
	}

	if( strcmp(argv[0], "reboot")==0 )
	{
		cmd_reboot(argc, argv);
		return;
	}
	if( strcmp(argv[0], "mot")==0 || strcmp(argv[0], "motor")==0 )
	{
		cmd_motor(argc, argv);
		return;
	}
	if( strcmp(argv[0], "set")==0 )
	{
		cmd_go(argc, argv);
		return;
	}
	if( strcmp(argv[0], "pwm")==0 )
	{
		cmd_pwm(argc, argv);
		return;
	}

	//not supported command
	if(argc>0)
		cmd_help();

}
