#include "define.h"
#include "usart_func.h"
//#include "mprintf.h"

extern UART_HandleTypeDef huart3;

/* for printf  ---------------------------------------------------------
*/
#ifdef __GNUC__
	#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
	#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

PUTCHAR_PROTOTYPE
{
	uart_putc(ch);
	return ch;
}

int uart_putc(int ch)
{
	if(HAL_UART_Transmit(&huart3, (uint8_t *)&ch, 1, 0xFFFF) != HAL_OK) return -1;
	if(ch==0x0a) uart_putc(0x0d);
	return 0;
}

/* uart rx queue ---------------------------------------------------------
*/
uint8_t 	g_uart0_rx_que[UART0_RX_QUE_SIZE];
uint16_t	g_uart0_rx_cnt;
uint16_t	g_uart0_rx_que_head;
uint16_t	g_uart0_rx_que_tail;

void reset_uart_rx_que(void)
{
	g_uart0_rx_cnt = 0;
	g_uart0_rx_que_head = 0;
	g_uart0_rx_que_tail = 0;
}

void dec_uart_rx_que(void)
{
	g_uart0_rx_que_tail++;
	g_uart0_rx_que_tail %= UART0_RX_QUE_SIZE;
	if(g_uart0_rx_cnt) g_uart0_rx_cnt--;
}

void inc_uart_rx_que(void)
{
	g_uart0_rx_que_head++;
	g_uart0_rx_que_head %= UART0_RX_QUE_SIZE;
//	if(++g_uart0_rx_cnt>=UART0_RX_QUE_SIZE) g_uart0_rx_cnt=UART0_RX_QUE_SIZE;
	if(++g_uart0_rx_cnt>=UART0_RX_QUE_SIZE) dec_uart_rx_que();
}

void push_uart_rx_que(uint8_t *p_rxd)
{
	g_uart0_rx_que[g_uart0_rx_que_head] = *p_rxd;
	inc_uart_rx_que();
}

int pop_uart_rx_que(uint8_t *c)
{
	if( g_uart0_rx_cnt==0 )
		return -1;

	*c = g_uart0_rx_que[g_uart0_rx_que_tail];
	dec_uart_rx_que();

	return 0;
}

//----------------------------------------------------------------------------
/* do not erase - for my reference
 * this function should be located at stm32f4xx_it.c
 */
#if 0
void USART1_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */
#ifdef FEATURE_LIGHT_UART
	// neil: my light irq handler, to avoid HAL handler (too heavy; sometimes dead)
	uint32_t tmp_flag = 0, tmp_source = 0;

	tmp_flag = __HAL_UART_GET_FLAG(&huart1, UART_FLAG_RXNE);
	tmp_source = __HAL_UART_GET_IT_SOURCE(&huart1, UART_IT_RXNE);
	if ((tmp_flag != RESET) && (tmp_source != RESET))
	{
		g_uart_rxd = (uint8_t)(huart1.Instance->RDR & (uint8_t)0x00FF);
		push_uart_rx_que(&g_uart_rxd);
	}
	__HAL_UART_CLEAR_PEFLAG(&huart1);

	return; // neil: finish using return to avoid HAL handler
#endif
  /* USER CODE END USART1_IRQn 0 */

	HAL_UART_IRQHandler(&huart1);
  /* USER CODE BEGIN USART1_IRQn 1 */
  /* USER CODE END USART1_IRQn 1 */
}
#endif
