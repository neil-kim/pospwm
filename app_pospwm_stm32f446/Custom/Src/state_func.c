#include "state_func.h"
#include "define.h"
#include "led_func.h"
#include "io_func.h"
#include "main.h"
#include "event_func.h"
#include "usart_func.h"


extern uint32_t state_tstamp;
extern state_t current_state;
extern st_status_t g_st;

// -------------------------------------------------------------
state_t state_ent_standby(event_t event)
{
	return NO_STATE_CHANGE;
}

state_t state_act_standby(event_t event)
{
	state_t ret;
	ret = NO_STATE_CHANGE;

	switch(event)
	{
		case EVENT_1:
			break;
		case EVENT_2:
			break;
		case EVENT_3:
			break;
		case EVENT_4:
			break;

		default:
			break;
	}

	return ret;
}

void state_ext_standby(event_t event)
{
}


// -------------------------------------------------------------
state_t state_ent_fast(event_t event)
{
	return NO_STATE_CHANGE;
}

state_t state_act_fast(event_t event)
{
	state_t ret;
	ret = NO_STATE_CHANGE;

	switch(event)
	{
		case EVENT_1:
			break;
		case EVENT_2:
			break;
		case EVENT_3:
			break;
		case EVENT_4:
			break;

		default:
			break;
	}

	return ret;
}

void state_ext_fast(event_t event)
{
}


// -------------------------------------------------------------
state_t state_ent_slow(event_t event)
{
	return NO_STATE_CHANGE;
}

state_t state_act_slow(event_t event)
{
	state_t ret;
	ret = NO_STATE_CHANGE;

	switch(event)
	{
		case EVENT_1:
			break;
		case EVENT_2:
			break;
		case EVENT_3:
			break;
		case EVENT_4:
			break;

		default:
			break;
	}

	return ret;
}

void state_ext_slow(event_t event)
{
}
