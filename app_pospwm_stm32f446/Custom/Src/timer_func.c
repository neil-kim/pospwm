#include "timer_func.h"
#include "event_func.h"
#include "define.h"

extern st_status_t g_st;

uint32_t get_systick(void)
{
	return HAL_GetTick();
}

void set_time_stamp(uint32_t *p)
{
	*p = HAL_GetTick();
}

uint32_t get_time_interval(uint32_t past_set_time_stamp)
{
	uint32_t curr_tick;

	curr_tick = HAL_GetTick();
	return (curr_tick - past_set_time_stamp);
}

void delay_ms(uint32_t msec)
{
	uint32_t curr_tick;

	curr_tick = get_systick();
	while(1)
	{
		if(get_time_interval(curr_tick) > msec)
			break;
	}
}


timer_status_t get_timer_status(timers_t timer_id)
{
	return g_st.timer[timer_id].timer_st;
}

void set_timer(timers_t timer_id, uint32_t count_ms, event_t timeout_event)
{
	if(timer_id <MAXNUM_TIMERs)
	{
		g_st.timer[timer_id].timeout_count = count_ms;
		g_st.timer[timer_id].timeout_event = timeout_event;
		g_st.timer[timer_id].timer_st = TIMER_ST_RUN;
	}
}

void reset_timer(timers_t timer_id)
{
	if(timer_id <MAXNUM_TIMERs)
	{
		g_st.timer[timer_id].timeout_count = 0;
		g_st.timer[timer_id].timeout_event = EVENT_STATE_POLL;
		g_st.timer[timer_id].timer_st = TIMER_ST_STOP;
	}
}

void reset_all_timers(void)
{
	int i;
	for(i=0; i<MAXNUM_TIMERs; i++)
	{
		reset_timer((timers_t)i);
	}
}

void pause_timer(timers_t timer_id)
{
	if(timer_id <MAXNUM_TIMERs)
	{
		if(g_st.timer[timer_id].timeout_count>0)
		{
			g_st.timer[timer_id].timer_st = TIMER_ST_PAUSED;
		}
		else
		{
			g_st.timer[timer_id].timer_st = TIMER_ST_STOP;
		}
	}
}

void resume_timer(timers_t timer_id)
{
	if(timer_id <MAXNUM_TIMERs)
	{
		if(g_st.timer[timer_id].timeout_count>0)
		{
			g_st.timer[timer_id].timer_st = TIMER_ST_RUN;
		}
		else
		{
			g_st.timer[timer_id].timer_st = TIMER_ST_STOP;
		}
	}
}

